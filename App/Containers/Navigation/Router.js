import React from 'react';
import {
  createBottomTabNavigator,
  createStackNavigator
} from "react-navigation";
import Ionicons from "react-native-vector-icons/Ionicons";
import HomeScreen from "../_TabHome/HomeScreen";
import Searchcreen from "../_TabSearch/Searchcreen";
import DetailScreen from "../Detail/DetailScreen";
import DescriptionScreen from "../Detail/components/DescriptionScreen";
// import DetailComponent from "../Detail/components/DetailComponent";

export const AppTab = createBottomTabNavigator(
  {
    Home: { screen: HomeScreen },
    Search: { screen: Searchcreen },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home`;
        } else if (routeName === 'Search') {
          iconName = `ios-search`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  }
);

export default createStackNavigator({
  AppTab: { screen: AppTab, navigationOptions: { title: "Estate App" } },
  DetailScreen: { screen: DetailScreen },
  DescriptionScreen: { screen: DescriptionScreen },
});
