import React, { PureComponent } from "react";
import {
  Picker,
  PickerIOS,
  Platform,
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet,
  Modal,
  Dimensions
} from "react-native";
// import { Metrics } from "../Themes";
import Colors from "../Colors";

const { width, height } = Dimensions.get('window')
class PickerComponent extends PureComponent {
  state = {
    value: this.props.selectedValue || "",
    isShowPicker: false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedValue) {
      this.setState({ value: nextProps.selectedValue });
    }
  }

  togglePicker = () => {
    this.setState({ isShowPicker: !this.state.isShowPicker });
  };

  handleLabel = data => {
    if (data.length === 0) return "";
    const newLabel = data.filter(item => item === this.state.value);
      return !!!newLabel[0] ? "Tất cả" : newLabel[0];
  };

  render() {
    const { text, data = [], enabled = true } = this.props;
    const { isShowPicker } = this.state;

    if (Platform.OS === "ios") {
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.setState({ isShowPicker: !isShowPicker });
            }}
          >
            <Text
              style={{ fontWeight: "bold", color: "black", marginVertical: 10 }}
            >
              {text}
            </Text>
            <View
              style={{
                borderWidth: 1,
                borderColor: "gray",
                borderRadius: 5,
                minHeight: 50,
                justifyContent: "center",
                paddingLeft: 7
              }}
            >
              <Text style={{ color: "black" }}>{this.handleLabel(data)}</Text>
            </View>
          </TouchableOpacity>
          <Modal
            animationType="fade"
            transparent
            visible={this.state.isShowPicker}
            onRequestClose={this.togglePicker}
          >
            <TouchableHighlight
              onPress={this.togglePicker}
              style={{
                ...StyleSheet.absoluteFillObject,
                justifyContent: "flex-end",
                alignItems: "flex-end",
                backgroundColor: "rgba(0,0,0,0.5)"
              }}
            >
              <View
                style={{
                  backgroundColor: "white",
                  width,
                  elevation: 5,
                  borderTopWidth: 1,
                  borderTopColor: Colors.border,
                  alignItems: "flex-end",
                  justifyContent: "center",
                  borderTopRightRadius: 5,
                  borderTopLeftRadius: 5
                }}
              >
                <View style={{ padding: 15 }}>
                  <TouchableOpacity onPress={this.togglePicker}>
                    <Text>Close</Text>
                  </TouchableOpacity>
                </View>
                <PickerIOS
                  selectedValue={this.state.value}
                  style={{
                    width,
                    backgroundColor: "white"
                  }}
                  onValueChange={itemValue =>
                    this.props.onChangeValue(itemValue)
                  }
                  mode="dropdown"
                >
                  {data.map((item, index) => (
                    <PickerIOS.Item
                      label={!!item ? item : "Tất cả"}
                      value={item}
                      key={index}
                    />
                  ))}
                </PickerIOS>
              </View>
            </TouchableHighlight>
          </Modal>
        </View>
      );
    }

    return (
      <View>
        <Text
          style={{ fontWeight: "bold", color: "#8e8e93", marginVertical: 10 }}
        >
          {text.toUpperCase()}
        </Text>
        <View
          style={{
            borderWidth: 1,
            borderColor: Colors.frost,
            borderRadius: 5,
            padding: 5
          }}
        >
          <Picker
            selectedValue={this.state.value}
            enabled={enabled}
            style={{ height: 50, width: width - 40 }}
            onValueChange={(itemValue, itemIndex) =>
              this.props.onChangeValue(itemValue, data[itemIndex].name)
            }
            mode="dropdown"
            itemStyle={{ color: "#8e8e93" }}
          >
            {data.map((item, index) => (
              <Picker.Item label={item.name} value={item._id} key={index} />
            ))}
          </Picker>
        </View>
      </View>
    );
  }
}

export default PickerComponent;
