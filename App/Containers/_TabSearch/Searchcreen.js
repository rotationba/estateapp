import React, { Component } from "react";
import {
  View,
  Text,
  Slider,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  FlatList
} from "react-native";
import Picker from "./Picker";
import Colors from "../Colors";
import HomeItem from "../_TabHome/HomeItem";
const { width, height } = Dimensions.get("window");
class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      minPriceValue: 200,
      maxPriceValue: 400,
      minAreaValue: 50,
      maxAreaValue: 200,
      loaibds: [],
      quanhuyen: [],
      loaibdsSelect: "",
      quanhuyenSelect: "",
      fetchingLoaibds: true,
      fetchingQuanhuyen: true,
      fetchingSearch: true,
      resultSearch: [],
      isShowSearchUI: true,
      priorityArea: "1",
      priorityPrice: "1",
      priorityLoaibds: "1",
      priorityQuanhuyen: "1",
    };
  }

  componentDidMount() {
    fetch("https://salty-temple-56923.herokuapp.com/loaibds")
      .then(res => {
        var loaibds = JSON.parse(res._bodyInit).data;
        // console.log(loaibds);
        this.setState({ loaibds, fetchingLoaibds: false });
      })
      .catch(e => {
        console.error(e);
        this.setState({ fetchingLoaibds: false });
      });
    fetch("https://salty-temple-56923.herokuapp.com/quanhuyen")
      .then(res => {
        var quanhuyen = JSON.parse(res._bodyInit).data;
        // console.log(loaibds);
        this.setState({ quanhuyen, fetchingQuanhuyen: false });
      })
      .catch(e => {
        console.error(e);
        this.setState({ fetchingQuanhuyen: false });
      });
  }

  updateLoaibds = loaibdsSelect => {
    this.setState({ loaibdsSelect });
  };
  updateQuanhuyen = quanhuyenSelect => {
    this.setState({ quanhuyenSelect });
  };

  handleSearch = () => {
    const {
      maxAreaValue,
      minAreaValue,
      maxPriceValue,
      minPriceValue,
      loaibdsSelect,
      quanhuyenSelect,
      priorityArea,
      priorityLoaibds,
      priorityPrice,
      priorityQuanhuyen,
    } = this.state;
    let priority = [parseInt(priorityPrice), parseInt(priorityLoaibds), parseInt(priorityArea), parseInt(priorityQuanhuyen)];
    console.log(priority);
    let jsonData = {
      data: {
        tongtienmin: minPriceValue,
        tongtienmax: maxPriceValue,
        loaibds: loaibdsSelect,
        dientichmin: minAreaValue,
        dientichmax: maxAreaValue,
        quanhuyen: quanhuyenSelect
      },
      priority
    };

    fetch("https://salty-temple-56923.herokuapp.com/search", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(jsonData)
    })
      .then(res => {
        console.log(JSON.parse(res._bodyInit).data);
        let resultSearch = JSON.parse(res._bodyInit).data;
        this.setState({ resultSearch, isShowSearchUI: false, fetchingSearch: false }, () => {
          if (this.state.resultSearch.length < 1) {
            alert("Không có dữ liệu phù hợp");
            this.setState({ isShowSearchUI: true })
          }
        });
      })
      .catch(e => console.error(e));
    console.log(jsonData);
  };

  updatePriorityArea = (priorityArea) => {
    this.setState({ priorityArea });
  }
  updatePriorityLoaibds = (priorityLoaibds) => {
    this.setState({ priorityLoaibds });
  }
  updatePriorityPrice = (priorityPrice) => {
    this.setState({ priorityPrice });
  }
  updatePriorityQuanhuyen = (priorityQuanhuyen) => {
    this.setState({ priorityQuanhuyen });
  }

  UISearch = () => {
    const {
      loaibds,
      quanhuyen,
      fetchingLoaibds,
      fetchingQuanhuyen
    } = this.state;
    let checkLoaibds = loaibds && loaibds.length > 0;
    let checkQuanhuyen = quanhuyen && quanhuyen.length > 0;
    let checkInit = !fetchingLoaibds && !fetchingQuanhuyen;
    const {
      container,
      wrapTitle,
      txtTitle,
      valueTitle,
      button,
      txtBtn
    } = styles;

    const priorities = ["1", "2", "3", "4"]
    return (
      <ScrollView style={container}>
        <View style={wrapTitle}>
          <Text style={txtTitle}> Giá tiền từ... </Text>
          <Text style={valueTitle}>
            {this.state.minPriceValue.toLocaleString()} triệu
          </Text>
        </View>
        <Slider
          minimumValue={200}
          maximumValue={3000}
          step={100}
          onValueChange={minPriceValue =>
            this.setState({ minPriceValue, maxPriceValue: minPriceValue * 2 })
          }
        />
        <View style={wrapTitle}>
          <Text style={txtTitle}> Đến ... </Text>
          <Text style={valueTitle}>
            {this.state.maxPriceValue.toLocaleString()} triệu
          </Text>
        </View>
        <Slider
          minimumValue={this.state.minPriceValue}
          maximumValue={this.state.minPriceValue * 10}
          step={100}
          onValueChange={maxPriceValue => this.setState({ maxPriceValue })}
        />
        <View style={wrapTitle}>
          <Text style={txtTitle}> Diện tích tiền từ... </Text>
          <Text style={valueTitle}>{this.state.minAreaValue} m2</Text>
        </View>
        <Slider
          minimumValue={50}
          maximumValue={300}
          step={20}
          onValueChange={minAreaValue =>
            this.setState({ minAreaValue, maxAreaValue: minAreaValue * 2 })
          }
        />
        <View style={wrapTitle}>
          <Text style={txtTitle}> Đến ... </Text>
          <Text style={valueTitle}>
            {this.state.maxAreaValue.toLocaleString()} m2
          </Text>
        </View>
        <Slider
          minimumValue={this.state.minAreaValue}
          maximumValue={this.state.minAreaValue * 5}
          step={20}
          onValueChange={maxAreaValue => this.setState({ maxAreaValue })}
        />
        {checkLoaibds && (
          <Picker
            text="Loại bất động sản"
            data={loaibds}
            selectedValue={this.state.loaibdsSelect}
            onChangeValue={value => this.updateLoaibds(value)}
          />
        )}
        {checkQuanhuyen && (
          <Picker
            text="Quận/Huyện"
            data={quanhuyen}
            selectedValue={this.state.quanhuyenSelect}
            onChangeValue={value => this.updateQuanhuyen(value)}
          />
        )}
        
        <Picker
          text="Độ ưu tiên cho tổng tiền"
          data={priorities}
          selectedValue={this.state.priorityPrice}
          onChangeValue={value => this.updatePriorityPrice(value)}
        />
        <Picker
          text="Độ ưu tiên cho loại bất động sản"
          data={priorities}
          selectedValue={this.state.priorityLoaibds}
          onChangeValue={value => this.updatePriorityLoaibds(value)}
        />
        <Picker
          text="Độ ưu tiên cho diện tích"
          data={priorities}
          selectedValue={this.state.priorityArea}
          onChangeValue={value => this.updatePriorityArea(value)}
        />
        <Picker
          text="Độ ưu tiên cho Quận/Huyện"
          data={priorities}
          selectedValue={this.state.priorityQuanhuyen}
          onChangeValue={value => this.updatePriorityQuanhuyen(value)}
        />
        <TouchableOpacity style={button} onPress={this.handleSearch}>
          <Text style={txtBtn}>{"Tìm kiếm".toLocaleUpperCase()}</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  };

  UIResult = () => {
    return (
      <View style={{ padding: 10}}>
        <FlatList
          ListHeaderComponent={() => (
            <TouchableOpacity style={styles.button} onPress={this.handleBackSearch}>
              <Text style={styles.txtBtn}>Trở lại</Text>
            </TouchableOpacity>
          )}
          keyExtractor={(item, index) => item._id.toString()}
          data={this.state.resultSearch}
          renderItem={({ item }) => (
            <HomeItem
              onPress={() => this.props.navigation.navigate("DetailScreen", item)}
              data={item}
            />
          )}
        />
      </View>
     
    );
  };

  handleBackSearch = () => {
    this.setState({ isShowSearchUI: true})
  }
  render() {
    const {
      fetchingLoaibds,
      fetchingQuanhuyen,
      isShowSearchUI,
      fetchingSearch
    } = this.state;
    let checkInit = !fetchingLoaibds && !fetchingQuanhuyen;
   
    return (
      <View style={{ flex: 1, backgroundColor: Colors.white }}>
        {isShowSearchUI ? (
          checkInit ? (
            this.UISearch()
          ) : (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          )
        ) : (
          !fetchingSearch && this.UIResult()
        )}
      </View>
    );
  }
}

const styles = {
  container: {
    backgroundColor: "#fff",
    minHeight: height - 200,
    padding: 10
  },
  wrapTitle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 5
  },
  txtTitle: {
    color: "green"
  },
  valueTitle: {
    borderColor: "blue",
    borderWidth: 1,
    padding: 3,
    borderRadius: 5,
    paddingHorizontal: 15,
    fontWeight: "bold"
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    backgroundColor: Colors.main,
    borderRadius: 7,
    marginTop: 20,
    marginBottom: 20
  },
  txtBtn: {
    color: Colors.black,
    fontSize: 20,
    fontWeight: "bold"
  }
};

export default SearchScreen;
