import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, StatusBar } from "react-native";
import RootRouter from "./Navigation/Router";

export default class App extends Component {
  componentDidMount() {
    StatusBar.setHidden(true);
  }
  render() {
    return (
      <View style={styles.container}>
        <RootRouter />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  xanh: {
    height: 50,
    width: 50,
    backgroundColor: "blue"
  },
  red: {
    height: 50,
    width: 50,
    backgroundColor: "red"
  },
  vang: {
    height: 50,
    width: 50,
    backgroundColor: "yellow"
  }
});
