import React, { Component } from "react";
import { View, Text, ScrollView, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

class DescriptionScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }

  componentDidMount() {
    // console.log(this.props)
    let data = this.props.navigation.state.params;
    console.log(data);
    this.setState({ data });
  }

  render() {
    const { data } = this.state;
    return (
      <ScrollView style={{ minHeight: height , backgroundColor: "#fff" }}>
        {data && <Text>{data.mota}</Text>}
      </ScrollView>
    );
  }
}

export default DescriptionScreen;
