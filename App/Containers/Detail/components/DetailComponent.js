import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity
} from "react-native";
import MapView from "react-native-maps";
import Swiper from "react-native-swiper";
import Ionicons from "react-native-vector-icons/Ionicons";
import numeral from "numeral";
import Colors from "../../Colors";
const { width, height } = Dimensions.get("window");

class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data } = this.props;
    const hinhanh = data.hinhanh;
    const map = data.map;
    const sodienthoai = data.sodienthoai;
    const {
      container,
      txtTitle,
      wrapSwiper,
      wrapItem,
      txtRight,
      txtLeft
    } = styles;
    return (
      <ScrollView style={container}>
        <Text style={txtTitle}>{data.title}</Text>
        <View style={wrapSwiper}>
          <Swiper>
            {hinhanh.map(e => (
              <Image
                key={e}
                style={{ height: 200, width: "100%" }}
                source={{ uri: e.trim() }}
              />
            ))}
          </Swiper>
        </View>
        <View style={wrapItem}>
          {/* <Ionicons name={'ios-paper'} size={25} color={Colors.windowTint} /> */}
          <Text style={txtLeft}>Tổng tiền</Text>
          <Text style={txtRight}>
            {numeral(data.tongtien).format(0, 0)} triệu
          </Text>
        </View>
        <View style={wrapItem}>
          <Text style={txtLeft}>Diện tích</Text>
          <Text style={txtRight}>{numeral(data.dientich).format(0, 0)} m2</Text>
        </View>
        <View style={wrapItem}>
          <Text style={txtLeft}>Tỉnh thành</Text>
          <Text style={txtRight}>{data.tinhthanh}</Text>
        </View>
        <View style={wrapItem}>
          <Text style={txtLeft}>Quận Huyện</Text>
          <Text style={txtRight}>{data.quanhuyen}</Text>
        </View>
        <View style={wrapItem}>
          <Text style={txtLeft}>Loại tin</Text>
          <Text style={txtRight}>{data.loaitin}</Text>
        </View>
        <View style={wrapItem}>
          <Text style={txtLeft}>Hướng</Text>
          <Text style={txtRight}>{data.huong}</Text>
        </View>
        <View style={wrapItem}>
          <Text style={txtLeft}>Người bán</Text>
          <Text style={txtRight}>{data.tennguoiban}</Text>
        </View>
        <View style={wrapItem}>
          <Text style={txtLeft}>Điện thoại</Text>
          <Text style={txtRight}>{sodienthoai[0]}</Text>
        </View>
        <View style={wrapItem}>
          <Text style={txtLeft}>Ngày đăng</Text>
          <Text style={txtRight}>{data.ngaydang.trim()}</Text>
        </View>
        <TouchableOpacity
          style={wrapItem}
          onPress={() =>
            this.props.navigation.navigate("DescriptionScreen", data)
          }
        >
          <Text style={txtLeft}>Mô tả chi tiết</Text>
          <Ionicons
            name={"ios-arrow-forward"}
            size={25}
            color={Colors.black}
            style={{ fontWeight: "bold" }}
          />
        </TouchableOpacity>
        <MapView
          style={styles.map}
          region={{
            latitude: parseFloat(map[0]) ? parseFloat(map[0]) : 16.0471659,
            longitude: parseFloat(map[1]) ? parseFloat(map[1]) : 108.1716864,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
          }}
        >
          <MapView.Marker
            coordinate={{
              latitude: parseFloat(map[0]) ? parseFloat(map[0]) : 16.0471659,
              longitude: parseFloat(map[1]) ? parseFloat(map[1]) : 108.1716864,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121
            }}
          />
        </MapView>
      </ScrollView>
    );
  }
}

const styles = {
  container: {
    minHeight: height,
    padding: 10,
    marginBottom: 50
  },
  txtTitle: {
    fontSize: 20,
    color: Colors.fire,
    paddingVertical: 10,
    fontWeight: "bold"
  },
  wrapSwiper: {
    width: "100%",
    height: 220
  },
  wrapItem: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomColor: Colors.cloud,
    borderBottomWidth: 1,
    paddingVertical: 10
  },
  txtRight: {
    color: Colors.main,
    fontSize: 15,
    fontWeight: "bold"
  },
  txtLeft: {
    color: Colors.black,
    fontSize: 15
  },

  map: {
    height: 200,
    width: width - 20,
    marginBottom: 15
  }
};

export default Detail;
