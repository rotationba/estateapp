import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  ActivityIndicator
} from "react-native";
import DetailComponet from "./components/DetailComponent";

const { width, height } = Dimensions.get("window");

class DetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      fetching: true
    };
  }
  static navigationOptions = {
    title: "Chi tiết"
  };

  componentDidMount() {
    const data = this.props.navigation.state.params;
    this.setState({ data, fetching: false });
  }
  componentWillUnmount() {
    this.setState({ fetching: true });
  }
  render() {
    const { data, fetching } = this.state;
    const { container } = styles;
    return (
      <View style={{ flex: 1 }}>
        {fetching ? (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        ) : (
          <ScrollView style={container}>
            <DetailComponet data={data} {...this.props} />
          </ScrollView>
        )}
      </View>
    );
  }
}

const styles = {
  container: {
    backgroundColor: "#fff",
    minHeight: height
  }
};

export default DetailScreen;
