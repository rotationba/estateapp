import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator } from "react-native";
import HomeItem from "./HomeItem";
class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      fetching: true,
      page: 1,
      refreshing: false
    };
  }

  static navigationOptions = {
    title: "Home"
  };

  getData = () => {
    fetch("https://salty-temple-56923.herokuapp.com/page/" + this.state.page)
      .then(res => {
        let arr = JSON.parse(res._bodyInit);
        this.setState({ data: [...this.state.data, ...arr.data], fetching: false, page: this.state.page + 1 });
      })
      .catch(e => {
        console.log(e);
        this.setState({ data: [...this.state.data], fetching: false });
      });
  }

  componentDidMount = async () => {
    this.getData();
  };
  handleLoadMore = () => {
    this.getData();
  }

  render() {
    const { data, fetching } = this.state;
    return (
      <View style={{ backgroundColor: "#fff", flex: 1 }}>
        {fetching && data.length > 0 ? (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        ) : (
          <FlatList
            keyExtractor={(item, index) => item._id.toString()}
            data={this.state.data}
            renderItem={({ item }) => (
              <HomeItem
                onPress={() =>
                  this.props.navigation.navigate("DetailScreen", item)
                }
                data={item}
              />
            )}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.2}
            ListFooterComponent={() => (
              <View
                style={{ heigth: 50, justifyContent: "center", alignItems: "center" }}
              >
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
            )}
          />
        )}
      </View>
    );
  }
}

export default HomeScreen;
