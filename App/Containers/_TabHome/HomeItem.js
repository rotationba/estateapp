import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";

class HomeItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { wrapper, wrapAvatar, wrapContent, txtTitle, txtPrice } = styles;
    const { data, onPress } = this.props;
    const hinhanh = data.hinhanh
    const isImage = hinhanh[0] && hinhanh[0]!== "";
    return (
      <TouchableOpacity style={wrapper} onPress={onPress}>
        <View style={wrapAvatar}>
          <Image
            style={{ height: 70, width: 70 }}
            source={isImage ? { uri: data.hinhanh[0] } : require('../../Images/icon.png')}
          />
        </View>
        <View style={wrapContent}>
          <Text style={txtTitle}>{data.title}</Text>
          <Text>{data.loaibds}</Text>
          <Text>{data.tennguoiban}</Text>
          <Text>{data.dientich} m2</Text>
          <Text style={txtPrice}>{data.tongtien} triệu</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = {
  wrapper: {
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "gray",
    borderBottomWidth: 1,
    marginVertical: 5,
    paddingBottom: 10
  },
  wrapAvatar: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
    padding: 10
  },
  wrapContent: {
    flex: 7
    // marginLeft: 15,
  },
  txtTitle: {
    fontSize: 19,
    fontWeight: "bold"
  },
  txtPrice: {
    color: 'red'
  }
};

export default HomeItem;
